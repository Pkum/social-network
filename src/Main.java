/**
 * Program to implement facebook application
 * @author Praveen
 */
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		 int choice;
		 //Create a scanner class
	     Scanner scn=new Scanner(System.in);
	     System.out.println("=======================Facebook Application1  Details========================");
	     System.out.println("1. Registration. ");
	     System.out.println("2. Login. ");
	     
	     //choose a choice either 1 or 2 
	     System.out.print("Enter your Choice:");
	     choice=scn.nextInt();
	     
	     //goto register method
	     if(choice==1){
	   	  LoginRegistration R=new LoginRegistration();
	         R.register();
	     }
	     //goto Login method
	     else if(choice==2){
	    	 LoginRegistration L=new LoginRegistration();
	         L.login();
	     }
	     else{
	         System.out.println("Choose only 1 or 2 option");
	     }

	}
	}


